#include "BxDF.h"
#include "SurfaceInteraction.h"
#include "Primitive.h"
#include "FresnelDialectric.h"

/// Lambertian

std::tuple<Vector3d, Vector3d, double> LambertianBxDF::sample_f(const Vector3d &wo) const {
    auto wi = cosineSampleHemisphere();
    if (wo.z < 0) wi.z *= -1;
    auto pdf_ = pdf(wo, wi);
    auto f_   = f(wo, wi);
    return std::make_tuple(f_, wi, pdf_);
}

Ray LambertianBxDF::generate_ray(const Ray &r, const SurfaceInteraction &interaction) const {
    // orthonormal coordinate frame (w,u,v)
    auto w = interaction.oriented_normal; 
    auto u = cross(fabs(w.x) > .1 ? Vector3d(0, 1, 0) : Vector3d(1, 0, 0), w).norm();
    auto v = cross(w, u);

    auto sample = interaction.object->sample();

    // random reflection ray
    auto d = (u * sample.first.point.x + 
              v * sample.first.point.y + 
              w * sample.first.point.z).norm();
    return Ray(interaction.point, d);
}

/// Specular

std::tuple<Vector3d, Vector3d, double> SpecularBxDF::sample_f(const Vector3d &woL) const {
    auto wiL = Vector3d(-woL.x, -woL.y, woL.z);
    auto ft  = color * fresnelDialectricEvaluate(fresnel1, fresnel2, wiL.z);
    return std::make_tuple(ft, wiL, 1);
}

Ray SpecularBxDF::generate_ray(const Ray &r, const SurfaceInteraction &interaction) const {
    auto ray = Ray(interaction.point, r.direction - interaction.normal * 2 * dot(interaction.normal, r.direction));
    return ray;
}

/// Oren-Nayar

Vector3d OrenNayarBxDF::f(const Vector3d &wo, const Vector3d &wi) const {
    auto sinThetaI = sqrt(std::max(0.0, 1 - wi.z * wi.z));
    auto sinThetaO = sqrt(std::max(0.0, 1 - wo.z * wo.z));
    
    auto maxCos = 0.0;
    if (sinThetaI > 1e-4 && sinThetaO > 1e-4) {
        auto sinPhiI = SinPhi(wi), 
             cosPhiI = CosPhi(wi);
        auto sinPhiO = SinPhi(wo),
             cosPhiO = CosPhi(wo);
        auto dCos = cosPhiI * cosPhiO + sinPhiI * sinPhiO;
        maxCos = std::max(0.0, dCos);
    }

    double sinAlpha, tanBeta;
    if (abs(wi.z) > abs(wo.z)) {
        sinAlpha = sinThetaO;
        tanBeta = sinThetaI / abs(wi.z);
    } else {
        sinAlpha = sinThetaI;
        tanBeta = sinThetaO / abs(wo.z);
    }
    return color * M_1_PI * (A + B * maxCos * sinAlpha * tanBeta);
}

std::tuple<Vector3d, Vector3d, double> OrenNayarBxDF::sample_f(const Vector3d &wo) const {
    auto wi = cosineSampleHemisphere();
    if (wo.z < 0) wi.z *= -1;
    auto pdf_ = pdf(wo, wi);
    auto f_   = f(wo, wi);
    return std::make_tuple(f_, wi, pdf_);
}

double OrenNayarBxDF::pdf(const Vector3d &wo, const Vector3d &wi) const {
    return 1.0;
}

Ray OrenNayarBxDF::generate_ray(const Ray &r, const SurfaceInteraction &interaction) const {
    // orthonormal coordinate frame (w,u,v)
    auto w = interaction.oriented_normal; 
    auto u = cross(fabs(w.x) > .1 ? Vector3d(0, 1, 0) : Vector3d(1, 0, 0), w).norm();
    auto v = cross(w, u);

    auto sample = interaction.object->sample();

    // random reflection ray
    auto d = (u * sample.first.point.x + 
              v * sample.first.point.y + 
              w * sample.first.point.z).norm();
    return Ray(interaction.point, d);
}