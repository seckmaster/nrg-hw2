cmake_minimum_required(VERSION 3.17)
project(HW2_path_tracer)

set(CMAKE_CXX_STANDARD 20)

add_executable(HW2_path_tracer 
    main.cpp
    Vector.h
    Ray.h
    SurfaceInteraction.h
    SurfaceInteraction.cpp
    Primitive.h
    Primitive.cpp
    Core.h
    Scene.h
    Bsdf.h
    BxDF.h
    BxDF.cpp
    Utils.h
    Samplers.h
    Samplers.cpp
    FresnelDialectric.h
    arg_parser.h
)