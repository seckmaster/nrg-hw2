#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <memory>

#include "arg_parser.h"
#include "Core.h"

using namespace std;

Vector3d radiance(const Ray &r, int depth, unsigned short *Xi, const Scene &scene, int E = 1) {
    /// @TODO: WE SHOULD AVOID DOING DYNAMIC CASTS BECAUSE
    /// THEY SERIOUSLY HINDER THE PEROFMANCE OF THE RENDERER !!

    auto query_intersection = scene.intersect(r);
    
    if (!query_intersection.has_value()) 
        return Vector3d();  // no intersection, return black

    auto interaction = query_intersection.value().first;
    auto t           = query_intersection.value().second;

    double p;
    if (interaction.color.x > interaction.color.y && interaction.color.x > interaction.color.z) {
        p = interaction.color.x;
    } else if (interaction.color.y > interaction.color.z) {
        p = interaction.color.y;
    } else {
        p = interaction.color.z;
    }
    

    auto light = dynamic_cast<const Light*>(interaction.object); /// @TODO:
    
    if (++depth > 5) {
        if (erand48(Xi) < p) {
            interaction.color = interaction.color * (1 / p);
        } else {
            if (light == nullptr) return Vector3d();
            return light->emission; // R.R.
        }
    }

    auto sample  = interaction.object->sample();

    // random reflection ray
    auto new_ray = sample.first.object->generate_ray(r, interaction);

    // Loop over any lights
    Vector3d e;
    if (!sample.first.object->has_specular_bxdf()) {
        for (auto it = scene.world.begin(); it != scene.world.end(); it++) {
            auto &shape = *it;
            
            auto light = dynamic_cast<Light*>(shape.get()); /// @TODO:
            if (light == nullptr) continue;

            Vector3d sw = light->shape->position - interaction.point,
                    su = cross(fabs(sw.x) > .1 ? Vector3d(0, 1, 0) : Vector3d(1, 0, 0), sw).norm(),
                    sv = cross(sw, su);
            auto cos_a_max = sqrt(
                1 -
                (dynamic_cast<const Sphere *>(light->shape.get()))->radius *
                    (dynamic_cast<const Sphere *>(light->shape.get()))
                        ->radius /
                    dot(interaction.point - light->shape->position,
                        interaction.point - light->shape->position));
            auto eps1 = erand48(Xi);
            auto eps2 = erand48(Xi);
            auto phi = 2 * M_PI * eps2;

            double cos_a = 1 - eps1 + eps1 * cos_a_max;
            double sin_a = sqrt(1 - cos_a * cos_a);
            
            auto l = su * cos(phi) * sin_a + sv * sin(phi) * sin_a + sw * cos_a;
            l.norm();
            
            auto intersect = scene.intersect(Ray(interaction.point, l));
            if (intersect.has_value() && intersect.value().first.object == shape.get()) {  // shadow ray
                auto omega = 2 * M_PI * (1 - cos_a_max);
                e = e + interaction.color * (light->emission * dot(l, interaction.oriented_normal) * omega) * M_1_PI;  // 1/pi for brdf
            }
        }
    }
    auto rad = radiance(new_ray, depth, Xi, scene, 1);
    auto emission = interaction.Le(Vector3d()); /// @TODO: 
    return emission * E + e + sample.first.color * rad;
    

    /// @TODO: Implement GLASS material

    // Ray reflRay(interaction.point, r.direction - interaction.normal * 2 * dot(interaction.normal, r.direction)); // Ideal dielectric REFRACTION
    // bool into = dot(interaction.normal, interaction.oriented_normal) > 0; // Ray from outside going in?
    // double nc = 1,
    //        nt = 1.5, 
    //        nnt = into ? nc / nt : nt / nc, 
    //        ddn = dot(r.direction, interaction.oriented_normal),
    //        cos2t;

    // if ((cos2t = 1 - nnt * nnt * (1 - ddn * ddn)) < 0)  // Total internal reflection
    //     return interaction.object->emission + interaction.wo * radiance(reflRay, depth, Xi, scene);

    // Vector3d tdir = (r.direction * nnt - interaction.normal * ((into ? 1 : -1) * (ddn * nnt + sqrt(cos2t)))).norm();
    // double a = nt - nc;
    // double b = nt + nc;
    // double R0 = a * a / (b * b);
    // double c = 1 - (into ? -ddn : dot(tdir, interaction.normal));
    // double Re = R0 + (1 - R0) * c * c * c * c * c;
    // double Tr = 1 - Re;
    // double P = .25 + .5 * Re;
    // double RP = Re / P;
    // double TP = Tr / (1 - P);

    // auto ewo = interaction.object->emission + interaction.wo;
    // Vector3d radiance_;
    // if (depth > 2) {
    //     if (erand48(Xi) < P) { // Russian roulette
    //         radiance_ = radiance(reflRay, depth, Xi, scene) * RP;
    //     } else {
    //         radiance_ = radiance(Ray(interaction.point, tdir), depth, Xi, scene) * TP;
    //     }
    // } else {
    //     radiance_ = radiance(reflRay, depth, Xi, scene) * Re + radiance(Ray(interaction.point, tdir), depth, Xi, scene) * Tr;
    // }
    // return ewo * radiance_;
}

void setup_scene(Scene &scene) {
    scene.add_shape(make_shared<Sphere>(1e5, Vector3d( 1e5+1,40.8,81.6),  make_shared<LambertianBxDF>(Vector3d(.75,.25,.25)))); //Left
    scene.add_shape(make_shared<Sphere>(1e5, Vector3d(-1e5+99,40.8,81.6), make_shared<LambertianBxDF>(Vector3d(.25,.25,.75)))); //Rght
    scene.add_shape(make_shared<Sphere>(1e5, Vector3d(50,40.8, 1e5),      make_shared<LambertianBxDF>(Vector3d(.75,.75,.75)))); //Back
    scene.add_shape(make_shared<Sphere>(1e5, Vector3d(50,40.8,-1e5+170),  make_shared<LambertianBxDF>())); //Frnt
    scene.add_shape(make_shared<Sphere>(1e5, Vector3d(50, 1e5, 81.6),     make_shared<LambertianBxDF>(Vector3d(.75,.75,.75)))); //Botm
    scene.add_shape(make_shared<Sphere>(1e5, Vector3d(50,-1e5+81.6,81.6), make_shared<LambertianBxDF>(Vector3d(.75,.75,.75)))); //Top

    ///
    scene.add_shape(make_shared<Sphere>(16.5, Vector3d(27,16.5,47), make_shared<SpecularBxDF>(Vector3d(1,1,1), 0.5, 1.2)));
    // scene.add_shape(make_shared<Sphere>(16.5, Vector3d(73,16.5,78), make_shared<LambertianBxDF>(Vector3d(0.25,0.25,0.75)*.999)));
    scene.add_shape(make_shared<Sphere>(16.5, Vector3d(73,16.5,78),  make_shared<OrenNayarBxDF>(Vector3d(0.75,0.25,0.25)*.999, 1.2)));
    // scene.add_shape(make_shared<AABB>  (Vector3d(10,10,10), Vector3d(40,16.5,120), make_shared<LambertianBxDF>(Vector3d(0.25,0.75,0.25)*.999)));
    
    ///
    // scene.add_shape(make_shared<DiffuseAreaLight>(
    //     1, Vector3d(1,1,1)*100,
    //     make_shared<Sphere>( // spherical
    //         1.5, 
    //         Vector3d(50, 81.6 - 16.5, 81.6),
    //         make_shared<LambertianBxDF>(Vector3d()))));

    scene.add_shape(make_shared<DiffuseAreaLight>(
        1, Vector3d(0.25,1,1)*100,
        make_shared<Sphere>( // spherical
            1.5, 
            Vector3d(20, 20.5, 140.6),
            make_shared<LambertianBxDF>(Vector3d(0.25,1,1)*0))));
    
    scene.add_shape(make_shared<DiffuseAreaLight>(
        1, Vector3d(1,1,0.25)*100,
        make_shared<Sphere>( // spherical
            1.5, 
            Vector3d(80, 20.5, 140.6),
            make_shared<LambertianBxDF>(Vector3d(1,1,0.25)*0))));
}

int main(int argc, char *argv[]) {
    stringstream ss;
    for (int i = 0; i < argc; i++) {
        ss << argv[i] << " ";
    }
    auto args = ss.str();

    int w = 1024, h = 768, samps = 1;
    string path = "image.ppm";
    
    {
        auto arg = parse_int(args, "width");
        if (arg.has_value()) { w = arg.value(); }
    }
    {
        auto arg = parse_int(args, "height");
        if (arg.has_value()) { h = arg.value(); }
    }
    {
        auto arg = parse_int(args, "samples");
        if (arg.has_value()) { samps = arg.value() / 4; }
    }
    {
        auto arg = parse_arg(args, "output_path");
        if (arg.has_value()) { path = arg.value(); }
    }
    
    /// Prepare scene
    Scene scene;
    setup_scene(scene);

    /// Camera
    Ray cam(Vector3d(50, 52, 295.6),  Vector3d(0, -0.042612, -1).norm());
    
    ///
    Vector3d cx = Vector3d(w * .5135 / h);
    Vector3d cy = cross(cx, cam.direction).norm() * .5135; 
    Vector3d r;
    Vector3d *c = new Vector3d[w * h];

    for (int y = 0; y < h; y++) {  // Loop over image rows
        fprintf(stderr, "\rRendering (%d spp) %5.2f%%", samps * 4, 100. * y / (h - 1));

        for (unsigned short x = 0, Xi[3] = {0, 0, static_cast<unsigned short> (y * y * y)}; x < w; x++) { // Loop cols
            for (int sy = 0, i = (h - y - 1) * w + x; sy < 2; sy++)  { // 2x2 subpixel rows
                for (int sx = 0; sx < 2; sx++, r = Vector3d()) {  // 2x2 subpixel cols
                    for (int s = 0; s < samps; s++) {
                        auto r1 = 2 * erand48(Xi);
                        auto r2 = 2 * erand48(Xi);

                        auto dx = r1 < 1 ? sqrt(r1) - 1 : 1 - sqrt(2 - r1);
                        auto dy = r2 < 1 ? sqrt(r2) - 1 : 1 - sqrt(2 - r2);
                        
                        auto d = cx * (((sx + .5 + dx) / 2 + x) / w - .5) +
                                 cy * (((sy + .5 + dy) / 2 + y) / h - .5) +
                                 cam.direction;

                        auto ray = Ray(cam.origin + d * 140, d.norm());
                        auto rad = radiance(ray, 0, Xi, scene) * (1. / samps);
                        r += rad;
                    }
                    c[i] = c[i] + Vector3d(clamp(r.x), clamp(r.y), clamp(r.z)) * .25;
                }
            }
        }
    }

    /// @TODO: Abstract file writing
    FILE *f = fopen(path.c_str(), "w");  // Write image to PPM file.
    fprintf(f, "P3\n%d %d\n%d\n", w, h, 255);
    for (int i = 0; i < w * h; i++) {
        fprintf(f, "%d %d %d ", toInt(c[i].x), toInt(c[i].y), toInt(c[i].z));
    }
}