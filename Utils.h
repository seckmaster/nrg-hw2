#ifndef _UTILS_H_
#define _UTILS_H_

#include <algorithm>

#include "Vector.h"

inline bool sameHemisphere(const Vector3d &w, const Vector3d &wp) {
    return w.z * wp.z > 0;
}

inline double clamp(double x, double min, double max) {
    return x < min ? min : x > max ? max : x;
}

inline double clamp(double x) { 
    return clamp(x, 0, 1);
}

inline int toInt(double x) { 
    return int(pow(clamp(x), 1 / 2.2) * 255 + .5); 
}

inline double to_rad(double deg) { return (M_PI / 180) * deg; }

inline double to_deg(double rad) { return (180 / M_PI) * rad; }

inline double sin2Theta(const Vector3d &w) {
    return std::max(0.0, 1.0 - w.z * w.z);
}

inline double CosPhi(const Vector3d &w) {
    double sinTheta = std::sqrt(sin2Theta(w));
    return (sinTheta == 0) ? 1 : clamp(w.x / sinTheta, -1, 1);
}

inline double SinPhi(const Vector3d &w) {
    double sinTheta = std::sqrt(sin2Theta(w));
    return (sinTheta == 0) ? 0 : clamp(w.y / sinTheta, -1, 1);
}


#endif /* _UTILS_H_ */