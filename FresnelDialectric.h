#ifndef _FRESNEL_DIALECTRIC_H_
#define _FRESNEL_DIALECTRIC_H_

#include <cmath>
#include <algorithm>

#include "Vector.h"
#include "Utils.h"

Vector3d fresnelDialectricEvaluate(double eta1, double eta2, double cosThetaI) {
    auto etaI = eta1;
    auto etaT = eta2;
    
    if (etaI == 0 && etaT == 0) {
        // special case when we don't want Fresnel, e.g. always want perfect reflection (mirror)
        return Vector3d(1,1,1);
    }

    cosThetaI = clamp(cosThetaI, -1, 1);
    // Potentially swap indices of refraction
    bool entering = cosThetaI > 0;
    
    if (!entering) {
        std::swap(etaI, etaT);
        cosThetaI = abs(cosThetaI);
    }

    // Compute _cosThetaT_ using Snell's law
    auto sinThetaI = sqrt(std::max(0.0, 1 - cosThetaI * cosThetaI));
    auto sinThetaT = etaI / etaT * sinThetaI;

    // Handle total public reflection
    if (sinThetaT >= 1) {
        return Vector3d(1,1,1);
    }

    auto cosThetaT = sqrt(std::max(0.0, 1 - sinThetaT * sinThetaT));
    auto Rparl = ((etaT * cosThetaI) - (etaI * cosThetaT)) /
                 ((etaT * cosThetaI) + (etaI * cosThetaT));
    auto Rperp = ((etaI * cosThetaI) - (etaT * cosThetaT)) /
                 ((etaI * cosThetaI) + (etaT * cosThetaT));
    auto val   = (Rparl * Rparl + Rperp * Rperp) / 2;
    return Vector3d(val, val, val);

}

#endif /* _FRESNEL_DIALECTRIC_H_ */