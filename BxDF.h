#ifndef _BxDF_
#define _BxDF_

#include "Vector.h"
#include "Utils.h"
#include "Samplers.h"
#include "Ray.h"

struct SurfaceInteraction;

struct BxDF {
    virtual bool is_specular() const = 0;
    virtual Vector3d f(const Vector3d &wo, const Vector3d &wi) const = 0;
    virtual std::tuple<Vector3d, Vector3d, double> sample_f(const Vector3d &woL) const = 0;
    virtual double pdf(const Vector3d &wo, const Vector3d &wi) const = 0;
    virtual Ray generate_ray(const Ray &r, const SurfaceInteraction &interaction) const = 0;
};

struct LambertianBxDF : public BxDF {
    LambertianBxDF() : color(Vector3d()) {}
    LambertianBxDF(const Vector3d &color) : color(color) {}

    bool is_specular() const {
        return false;
    }

    Vector3d f(const Vector3d &wo, const Vector3d &wi) const {
        // if (!sameHemisphere(wo, wi)) return Vector3d();
        return color;
    }
    
    std::tuple<Vector3d, Vector3d, double> sample_f(const Vector3d &wo) const;

    double pdf(const Vector3d &wo, const Vector3d &wi) const {
        // if (!sameHemisphere(wo, wi)) return 0;
        return abs(wi.z) * M_1_PI;
    }

    Ray generate_ray(const Ray &r, const SurfaceInteraction &interaction) const;

    /// Data

    Vector3d color;
};

struct SpecularBxDF : public BxDF {
    SpecularBxDF(Vector3d color, double fresnel1, double fresnel2)
        : color(color), fresnel1(fresnel1), fresnel2(fresnel2) {}

    bool is_specular() const {
        return true;
    }

    Vector3d f(const Vector3d &wo, const Vector3d &wi) const {
        return Vector3d();
    }

    std::tuple<Vector3d, Vector3d, double> sample_f(const Vector3d &woL) const;

    double pdf(const Vector3d &wo, const Vector3d &wi) const {
        return 0;
    }

    Ray generate_ray(const Ray &r, const SurfaceInteraction &interaction) const;

    /// Data

    Vector3d color;
    double fresnel1, fresnel2;
};

struct OrenNayarBxDF : public BxDF {
    OrenNayarBxDF(const Vector3d &color, double sig) : color(color) {
        sig = to_rad(sig);
        auto sigma2 = sig * sig;
        A = 1. - (sigma2 / (2.0 * (sigma2 + 0.33)));
        B = 0.45 * sigma2 / (sigma2 + 0.09);
    }

    bool is_specular() const { return false; }

    Vector3d f(const Vector3d &wo, const Vector3d &wi) const;
    std::tuple<Vector3d, Vector3d, double> sample_f(const Vector3d &woL) const;
    double pdf(const Vector3d &wo, const Vector3d &wi) const;
    Ray generate_ray(const Ray &r, const SurfaceInteraction &interaction) const;

    /// Data

    Vector3d color;
    double A, B;
};

#endif /* _BxDF_ */