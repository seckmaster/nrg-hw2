#ifndef _CORE_H_
#define _CORE_H_

#include "Ray.h"
#include "Primitive.h"
#include "SurfaceInteraction.h"
#include "Scene.h"
#include "Samplers.h"

#endif 