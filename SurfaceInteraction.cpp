#include "SurfaceInteraction.h"
#include "Primitive.h"

Vector3d SurfaceInteraction::Le(const Vector3d &w) const {
    auto light = dynamic_cast<const Light*>(object); /// @TODO: avoid dynamic_cast ??
    if (light == nullptr) return Vector3d();
    return light->L(*this, w);
}