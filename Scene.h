#ifndef _SCENE_H
#define _SCENE_H

#include <memory>
#include <vector>
#include <optional>

#include "Primitive.h"
#include "SurfaceInteraction.h"

struct Scene {
    inline void add_shape(std::shared_ptr<Primitive> shape) {
        world.push_back(shape);
    }

    inline std::optional<std::pair<SurfaceInteraction, double>> intersect(const Ray &r) const {
        double inf = 1e20;
        double t   = 1e20;
        SurfaceInteraction interaction;
        std::shared_ptr<Primitive> obj;

        for (auto it = world.begin(); it != world.end(); it++) {
            auto intersect = (*it)->intersect(r);
            if (intersect.has_value() && intersect.value().second < t) {
                t = intersect.value().second;
                interaction = intersect.value().first;
            }
        }
        if (t >= inf) {
            return {};
        }

        return std::make_pair(interaction, t);
    }

    /// Data
    
    typedef std::vector<std::shared_ptr<Primitive>> World;
    World world;
};

#endif /* _SCENE_H */