#ifndef _SURFACE_INTERACTION_H
#define _SURFACE_INTERACTION_H

#include "Vector.h"

struct Primitive;

struct SurfaceInteraction {
    Vector3d point;
    Vector3d normal;
    Vector3d oriented_normal;
    Vector3d Dpdu;
    Vector3d Dpdv;
    Vector3d color;
    Primitive const *object; /// TODO: CAN WE USE SMART PTR INSTREAD OF RAW PTR ??

    Vector3d Le(const Vector3d &w) const;
};

#endif /* _SURFACE_INTERACTION_H */