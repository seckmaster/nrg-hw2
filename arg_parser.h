//
// Created by Toni Kocjan on 04/03/2021.
//

#ifndef HW1_ARG_PARSER_H
#define HW1_ARG_PARSER_H

#include <optional>
#include <string>

std::optional<std::string> parse_arg(const std::string& args,
                                     const std::string& arg_name) {
    auto index = args.find(arg_name);
    if (index == std::string::npos) {
        return {};
    }
    auto start_index = index + arg_name.size() + 1;
    if (start_index >= args.size()) {
        return {};
    }
    auto end_index = args.find(' ', start_index);
    if (end_index == std::string::npos) {
        end_index = args.size();
    }
    auto arg_value = args.substr(start_index, end_index - start_index);
    return arg_value;
}

std::optional<double> parse_double(const std::string& args,
                                   const std::string& arg_name) {
    auto optional_arg_value = parse_arg(args, arg_name);
    if (optional_arg_value) {
        try {
            auto double_value = std::stod(*optional_arg_value);
            return double_value;
        } catch (const std::invalid_argument&) {
            return {};
        } catch (const std::out_of_range&) {
            return {};
        }
    }
    return {};
}

std::optional<int> parse_int(const std::string& args,
                             const std::string& arg_name) {
    auto optional_arg_value = parse_arg(args, arg_name);
    if (optional_arg_value) {
        try {
            auto int_value = std::stoi(*optional_arg_value);
            return int_value;
        } catch (const std::invalid_argument&) {
            return {};
        } catch (const std::out_of_range&) {
            return {};
        }
    }
    return {};
}

std::optional<bool> parse_bool(const std::string& args,
                               const std::string& arg_name) {
    auto optional_arg_value = parse_arg(args, arg_name);
    if (optional_arg_value) {
        if (*optional_arg_value == "1" || *optional_arg_value == "true") {
            return true;
        }
        if (*optional_arg_value == "0" || *optional_arg_value == "false") {
            return false;
        }
    }
    return {};
}


// -------

enum Method {
    BASIC, MODIFIED, MODIFIED2
};

std::optional<Method> parse_method(const std::string& args,
                                   const std::string& arg_name) {
    auto optional_arg = parse_arg(args, arg_name);
    if (optional_arg) {
        if (*optional_arg == "basic") {
            return BASIC;
        }
        if (*optional_arg == "modified") {
            return MODIFIED;
        }
        if (*optional_arg == "modified2") {
            return MODIFIED2;
        }
    }
    return {};
}

struct Parameters {
    Method method;
    float min_x, max_x;
    float min_y, max_y;
    float min_z, max_z;
    int res_x, res_y, res_z;

    union {
        float p;
        float R;
    };
};

Parameters parse_parameters(const std::string& args) {
    Parameters parameters;
    parameters.method = *parse_method(args, "--method");
    parameters.min_x = *parse_double(args, "--min-x");
    parameters.max_x = *parse_double(args, "--max-x");
    parameters.min_y = *parse_double(args, "--min-y");
    parameters.max_y = *parse_double(args, "--max-y");
    parameters.min_z = *parse_double(args, "--min-z");
    parameters.max_z = *parse_double(args, "--max-z");
    parameters.res_x = *parse_int(args, "--res-x");
    parameters.res_y = *parse_int(args, "--res-y");
    parameters.res_z = *parse_int(args, "--res-z");
    if (parameters.method == BASIC) {
        parameters.p = *parse_double(args, "--p");
    } else {
        parameters.R = *parse_double(args, "--r");
    }
    return parameters;
}

#endif //HW1_ARG_PARSER_H
