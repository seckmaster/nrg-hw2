#include <utility> 
#include <memory>

#include "SurfaceInteraction.h"
#include "Primitive.h"

using namespace std;

pair<SurfaceInteraction, double> Shape::sample(const SurfaceInteraction &interaction) const {
    auto sample = this->sample();
    auto wi = sample.first.point - interaction.point;
    if (wi.lengthSquared() < 10e-5)
        sample.second = 0;
    else {
        wi.norm();
        sample.second *= (interaction.point - sample.first.point).lengthSquared() / abs(dot(sample.first.normal, wi * -1));
        if (isinf(sample.second) || isnan(sample.second))
            sample.second = 0;
    }
    return sample;
}

/// Sphere

std::optional<std::pair<SurfaceInteraction, double>> Sphere::intersect(const Ray &r) const {
    // Solve t^2*d.d + 2*t*(o-p).d + (o-p).(o-p)-R^2 = 0

    auto op    = position - r.origin;
    double eps = 1e-4; 
    double b   = dot(op, r.direction);
    
    double det = b * b - dot(op, op) + radius * radius;
    
    if (det < 0) {
        return {};
    } else {
        det = sqrt(det);
    }

    double t;

    if (b - det > eps) {
        t = b - det;        
    } else if (b + det > eps) {
        t = b + det;
    } else {
        return {};
    }

    auto point           = r.point(t);
    auto normal          = (point - position).norm();
    auto oriented_normal = dot(normal, r.direction) < 0 ? normal : normal * -1;
    auto color           = sample().first.color;

    SurfaceInteraction interaction = {
        point,
        normal,
        oriented_normal,
        Vector3d(), // dpdu????
        Vector3d(), // dpdu????
        color,
        this
    };

    return make_pair(interaction, t);
}

pair<SurfaceInteraction, double> Sphere::sample() const {
    assert(bsdf.bxdfs.size() == 1); // one bxdf supported atm
    
    auto p      = uniformSampleSphere() * radius;
    auto normal = Vector3d(p.x,p.y,p.z).norm();
    p = p * (radius / p.length());

    /// @TODO: Use all BxDFs
    auto bxdf   = bsdf.bxdfs.front();
    auto sample = bxdf->sample_f(p); // TODO: what should be the argument?

    auto interaction = SurfaceInteraction {
        // Vector3d point;
        // Vector3d normal;
        // Vector3d oriented_normal;
        // Vector3d Dpdu;
        // Vector3d Dpdv;
        // Vector3d wo;
        // Shape *object;
        
        get<1>(sample),
        normal,
        normal,
        Vector3d(), // @TODO
        Vector3d(), // @TODO
        get<0>(sample),
        this
    };
    return make_pair(interaction, 0.0);
}

std::optional<std::pair<SurfaceInteraction, double>> AABB::intersect(const Ray &r) const {
    // if (r.direction.z == 0) { return {}; }
    // auto shape_hit = -r.origin.z / r.direction.z;
    // if (shape_hit <= 10e-4) { return {}; }
    // auto hit = r.point(shape_hit);
    // if (hit.x<-size.x*0.5 || hit.x>size.x*0.5 || 
    //     hit.y<-size.y*0.5 || hit.y>size.y*0.5 ||
    //     hit.z<-size.z*0.5 || hit.z>size.z*0.5) { return {}; }
    // hit.z = 0;
    // auto interaction = SurfaceInteraction {
    //     pHit, 
    //     Vector3(0, 0, 1),
    //     Vector3(0, 0, 1),
    //     -r.d,
    //     Vector3(1, 0, 0),
    //     this
    // };
    // return make_pair(interaction, shape_hit);

    float tmin, tmax, tymin, tymax, tzmin, tzmax; 
    auto inv_dir = r.inv_direction;

    Vector3d bounds[2] = {
        position,
        position + size
    };
 
    tmin  = (bounds[r.sign[0]].x - r.origin.x) * inv_dir.x; 
    tmax  = (bounds[1-r.sign[0]].x - r.origin.x) * inv_dir.x; 
    tymin = (bounds[r.sign[1]].y - r.origin.y) * inv_dir.y; 
    tymax = (bounds[1-r.sign[1]].y - r.origin.y) * inv_dir.y; 
 
    if ((tmin > tymax) || (tymin > tmax)) 
        return {};
    if (tymin > tmin) 
        tmin = tymin; 
    if (tymax < tmax) 
        tmax = tymax; 
 
    tzmin = (bounds[r.sign[2]].z - r.origin.z) * inv_dir.z; 
    tzmax = (bounds[1-r.sign[2]].z - r.origin.z) * inv_dir.z; 
 
    if ((tmin > tzmax) || (tzmin > tmax)) 
        return {}; 
    if (tzmin > tmin) 
        tmin = tzmin; 
    if (tzmax < tmax) 
        tmax = tzmax; 

    auto interaction = SurfaceInteraction {
        r.point(tmin),
        Vector3d(1,0,0),
        Vector3d(1,0,0),
        Vector3d(),
        Vector3d(),
        sample().first.color,
        this
    };
 
    return make_pair(interaction, tmin); 
}

std::pair<SurfaceInteraction, double> AABB::sample() const {
    auto point = sampleCube() * size;
    auto bxdf   = bsdf.bxdfs.front();
    auto sample = bxdf->sample_f(Vector3d(1,1,1)); // TODO: what should be the argument?
    auto interaction = SurfaceInteraction {
        // Vector3d point;
        // Vector3d normal;
        // Vector3d oriented_normal;
        // Vector3d Dpdu;
        // Vector3d Dpdv;
        // Vector3d wo;
        // Shape *object;
        
        get<1>(sample),
        Vector3d(), // @TODO
        Vector3d(), // @TODO
        Vector3d(), // @TODO
        Vector3d(), // @TODO
        get<0>(sample),
        this
    };
    return make_pair(interaction, 0.0);
}

/// Light(s)

tuple<Vector3d, Vector3d, double, Vector3d> DiffuseAreaLight::sample_li(const SurfaceInteraction &source) const {
    auto sample = shape->sample(source);
    auto wi     = (sample.first.point - source.point).norm();
    auto Li     = L(sample.first, wi * -1);
    return make_tuple(Li, wi, 0.0, sample.first.point);;
}

Vector3d DiffuseAreaLight::L(const SurfaceInteraction &si, const Vector3d &w) const {
    /// @TODO: 
    // return (dot(si.oriented_normal, w) > 0) ? emission : Vector3d();
    return emission;
}