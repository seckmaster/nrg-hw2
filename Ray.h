#ifndef _RAY_H_
#define _RAY_H_

#include "Vector.h"

struct Ray {
    Ray(Vector3d oo, Vector3d dd) : origin(oo), direction(dd) {
        inv_direction = 1 / direction;
        sign[0] = (inv_direction.x < 0); 
        sign[1] = (inv_direction.y < 0); 
        sign[2] = (inv_direction.z < 0); 
    }

    inline Vector3d point(double t) const {
        return origin + direction * t;
    }

    /// Data

    Vector3d origin;
    Vector3d direction;
    Vector3d inv_direction;
    unsigned short sign[3];
};

#endif /* _RAY_H_ */