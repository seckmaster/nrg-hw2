#ifndef _PRIMITIVE_H_
#define _PRIMITIVE_H_

#include <optional>

#include "Ray.h"
#include "Bsdf.h"
#include "SurfaceInteraction.h"

/// Primitive interface

struct Primitive {
    virtual std::optional<std::pair<SurfaceInteraction, double>> intersect(const Ray &r) const = 0;
    virtual std::pair<SurfaceInteraction, double> sample() const = 0;
    virtual Ray generate_ray(const Ray &ray, const SurfaceInteraction &interaction) const = 0;
    virtual bool has_specular_bxdf() const = 0; /// TODO: needed atm, but should be removed in the future
    // virtual ~Primitive(); /// @TODO
};

/// Shape abstract class

struct Shape : public Primitive {
    Shape(const Vector3d &position, std::shared_ptr<BxDF> bxdf)
        : position(position) {
        bsdf.add_bxdf(bxdf);
    }

    // virtual ~Shape(); /// @TODO

    virtual std::pair<SurfaceInteraction, double> sample() const = 0;
    std::pair<SurfaceInteraction, double> sample(const SurfaceInteraction &interaction) const;
    
    Ray generate_ray(const Ray &ray, const SurfaceInteraction &interaction) const {
        assert(!bsdf.bxdfs.empty());
        return bsdf.bxdfs[0]->generate_ray(ray, interaction);
    }

    bool has_specular_bxdf() const {
        assert(!bsdf.bxdfs.empty());
        return bsdf.bxdfs[0]->is_specular();
    }

    /// Data

    Vector3d position;
    BSDF bsdf;
};

/// Sphere

struct Sphere : public Shape {
    Sphere(double radius, const Vector3d &position, std::shared_ptr<BxDF> bxdf)
        : Shape(position, bxdf), radius(radius) {}

    // ~Sphere() {} /// @TODO

    std::optional<std::pair<SurfaceInteraction, double>> intersect(const Ray &r) const;
    std::pair<SurfaceInteraction, double> sample() const;

    /// Data

    double radius;
};

/// AABB
/// @TODO: Not working correctly ...

struct AABB : public Shape {
    AABB(const Vector3d &size, const Vector3d &position, std::shared_ptr<BxDF> bxdf)
        : Shape(position, bxdf), size(size) {}

    // ~AABB() {} /// @TODO

    std::optional<std::pair<SurfaceInteraction, double>> intersect(const Ray &r) const;
    std::pair<SurfaceInteraction, double> sample() const;

    /// Data

    Vector3d size;
};

/// Light abstract class

struct Light : public Primitive {
    Light(const Vector3d &emission, std::shared_ptr<Shape> shape) : emission(emission), shape(shape) {
    }

    virtual std::optional<std::pair<SurfaceInteraction, double>> intersect(const Ray &r) const {
        auto intersect = shape->intersect(r);
        if (!intersect.has_value()) return {};
        intersect.value().first.object = this;
        return intersect;
    }

    virtual std::pair<SurfaceInteraction, double> sample() const {
        return shape->sample();
    }
    
    virtual std::tuple<Vector3d, Vector3d, double, Vector3d> sample_li(const SurfaceInteraction &source) const = 0;
    virtual Vector3d L(const SurfaceInteraction &si, const Vector3d &w) const = 0;

    Ray generate_ray(const Ray &ray, const SurfaceInteraction &interaction) const {
        return shape->generate_ray(ray, interaction);
    }

    bool has_specular_bxdf() const {
        return shape->has_specular_bxdf();
    }

    /// Data

    Vector3d emission;
    std::shared_ptr<Shape> shape;
};

/// Diffuse area light

struct DiffuseAreaLight : public Light {
    DiffuseAreaLight(double intensity, const Vector3d &emission,
                     std::shared_ptr<Shape> shape)
        : Light(emission, shape), intensity(intensity) {}

    std::tuple<Vector3d, Vector3d, double, Vector3d> sample_li(const SurfaceInteraction &source) const;
    Vector3d L(const SurfaceInteraction &si, const Vector3d &w) const;
    
    /// Data

    double intensity;
};

#endif /* _PRIMITIVE_H_ */