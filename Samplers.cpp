#include "Samplers.h"

Vector3d uniformSampleSphere() {
    double eps1  = erand48(DEF_SEED_ERAND48);
    double eps2  = erand48(DEF_SEED_ERAND48);
    double theta = 2 * M_PI * eps2;
    double phi   = acos(1 - eps1 * eps1);
    return Vector3d(sin(phi) * cos(theta), sin(phi) * sin(theta), cos(phi));
}

std::pair<double, double> uniformSampleSquare() {
    auto u = erand48(DEF_SEED_ERAND48);
    auto v = erand48(DEF_SEED_ERAND48);
    return std::make_pair(u, v);
}

std::pair<double, double> uniformSampleDisk() {
    auto u = erand48(DEF_SEED_ERAND48);
    auto v = erand48(DEF_SEED_ERAND48);
    auto r = sqrt(u);
    auto theta = 2 * M_PI * v;
    return std::make_pair(r * cos(theta), r * sin(theta));
}

Vector3d cosineSampleHemisphere() {
    auto disk = uniformSampleDisk();
    auto z = sqrt(std::max(0.0, 1.0 - disk.first * disk.first - disk.second * disk.second));
    return Vector3d(disk.first, disk.second, z);
}

Vector3d sampleCube() {
    double x = erand48(DEF_SEED_ERAND48);
    double y = erand48(DEF_SEED_ERAND48);
    double z = erand48(DEF_SEED_ERAND48);
    return Vector3d(x,y,z);
}