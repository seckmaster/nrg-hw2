#ifndef _SAMPLERS_H_
#define _SAMPLERS_H_

#include <stdlib.h>
#include <cmath>
#include <algorithm>
#include <utility>

#include "Vector.h"

static unsigned short DEF_SEED_ERAND48[3] = {1231, 10012, 123};

Vector3d uniformSampleSphere();
std::pair<double, double> uniformSampleSquare();
std::pair<double, double> uniformSampleDisk();
Vector3d cosineSampleHemisphere();
Vector3d sampleCube();

#endif /* _SAMPLERS_H_ */