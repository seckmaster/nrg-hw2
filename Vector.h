#ifndef _VECTOR_H_
#define _VECTOR_H_

#include <cmath>

template <typename T>
struct Vector {
    /// Data

    T x, y, z;

    ///

    // Vector() {
    //     x = 0;
    //     y = 0;
    //     z = 0;
    // }
    // Vector(T xx, T yy, T zz) {
    //     x = xx;
    //     y = yy;
    //     z = zz;
    // }
    // Vector(T xyz) {
    //     x = xyz;
    //     y = xyz;
    //     z = xyz;
    // }
    Vector(T x_ = 0, T y_ = 0, T z_ = 0) {
        x = x_;
        y = y_;
        z = z_;
    }

    Vector operator+(const Vector &b) const { return Vector(x + b.x, y + b.y, z + b.z); }

    void operator+=(const Vector &b) { x += b.x; y += b.y; z += b.z; }

    Vector operator-(const Vector &b) const { return Vector(x - b.x, y - b.y, z - b.z); }

    Vector operator*(double b) const { return Vector(x * b, y * b, z * b); }

    Vector operator/(double b) const { return Vector(x / b, y / b, z / b); }

    Vector operator*(const Vector &b) const { return Vector(x * b.x, y * b.y, z * b.z); }

    Vector &norm() { return *this = *this * (1 / sqrt(x * x + y * y + z * z)); }

    T length() const { return sqrt(x * x + y * y + z * z); }

    T lengthSquared() const { return pow(sqrt(x * x + y * y + z * z), 2); }
};

template <typename T>
double dot(const Vector<T> &a, const Vector<T> &b) {
    return a.x * b.x + a.y * b.y + a.z * b.z;
}

template <typename T>
Vector<T> cross(const Vector<T> &a, const Vector<T> &b) {
    return Vector(a.y * b.z - a.z * b.y, 
                  a.z * b.x - a.x * b.z,
                  a.x * b.y - a.y * b.x);
}

template <typename T>
Vector<T> operator/(double b, const Vector<T> &a) { return Vector<T>(b / a.x, b / a.y, b / a.z); }

typedef Vector<double> Vector3d;

#endif /* _VECTOR_H_ */