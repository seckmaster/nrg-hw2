#ifndef _BSDF_H_
#define _BSDF_H_

#include <vector>
#include <memory>

#include "BxDF.h"

struct BSDF {
    inline void add_bxdf(std::shared_ptr<BxDF> bxdf) {
        /// @TODO: SUPPORT MULTIPLE BxDFs in future
        assert(bxdfs.empty());

        bxdfs.push_back(bxdf);
    }

    /// Data

    std::vector<std::shared_ptr<BxDF>> bxdfs;
};

#endif /* _BSDF_H_ */